package pl.pjwstk.ordermanager.domain;

public enum EntityState {
    NEW, CHANGED, UNCHANGED, DELETED
}
