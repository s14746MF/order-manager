package pl.pjwstk.ordermanager.domain;

public class Entity {

    private Long id;

    EntityState state;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public EntityState getState() {
        return state;
    }

    public void setState(EntityState state) {
        this.state = state;
    }
}
