package pl.pjwstk.ordermanager.domain;

import java.util.ArrayList;
import java.util.List;

public class Order extends Entity {

    private ClientDetails client;
    private Address deliveryAddress;
    private List<OrderItem> items = new ArrayList<>();

    public ClientDetails getClient() {
        return client;
    }

    public void setClient(ClientDetails client) {
        this.client = client;
    }

    public Address getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(Address deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public List<OrderItem> getItems() {
        return items;
    }

    public void addItem(OrderItem orderItem) {
        items.add(orderItem);
    }

    public void addItems(List<OrderItem> orderItems) {
        items.addAll(orderItems);
    }
}
