package pl.pjwstk.ordermanager.repository;

import pl.pjwstk.ordermanager.builder.EntityBuilder;
import pl.pjwstk.ordermanager.domain.OrderItem;
import pl.pjwstk.ordermanager.unitofwork.UnitOfWork;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class OrderItemRepository extends RepositoryImpl<OrderItem> {

    protected OrderItemRepository(Connection connection, EntityBuilder<OrderItem> builder, UnitOfWork uow) {
        super(connection, builder, uow);
    }

    protected void setUpUpdateQuery(OrderItem entity) throws SQLException {
        update.setLong(1, entity.getOrderId());
        update.setString(2, entity.getName());
        update.setString(3, entity.getDescription());
        update.setDouble(4, entity.getPrice());
        update.setLong(5, entity.getId());
    }

    protected void setUpInsertQuery(OrderItem entity) throws SQLException {
        insert.setLong(1, entity.getOrderId());
        insert.setString(2, entity.getName());
        insert.setString(3, entity.getDescription());
        insert.setDouble(4, entity.getPrice());
    }

    protected String getTableName() {
        return "ORDER_ITEMS";
    }

    protected String getUpdateQuery() {
        return "UPDATE ORDER_ITEMS SET (order_id, name, description, price) = (?, ?, ?, ?) WHERE id = ?";
    }

    protected String getInsertQuery() {
        return "INSERT INTO ORDER_ITEMS (order_id, name, description, price) VALUES (?, ?, ?, ?)";
    }

    public List<OrderItem> getAllByOrderId(Long orderId) {
        List<OrderItem> orderItems = new ArrayList<>();

        try {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM ORDER_ITEMS WHERE order_id = ?");
            statement.setLong(1, orderId);

            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                orderItems.add(builder.build(resultSet));
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }

        return orderItems;
    }
}
