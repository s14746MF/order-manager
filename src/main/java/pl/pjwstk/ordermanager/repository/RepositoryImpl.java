package pl.pjwstk.ordermanager.repository;

import pl.pjwstk.ordermanager.domain.Entity;
import pl.pjwstk.ordermanager.builder.EntityBuilder;
import pl.pjwstk.ordermanager.unitofwork.UnitOfWork;
import pl.pjwstk.ordermanager.unitofwork.UnitOfWorkRepository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public abstract class RepositoryImpl<TEntity extends Entity> implements Repository<TEntity>, UnitOfWorkRepository {

    protected UnitOfWork uow;
    protected Connection connection;
    protected PreparedStatement selectByID;
    protected PreparedStatement insert;
    protected PreparedStatement delete;
    protected PreparedStatement update;
    protected PreparedStatement selectAll;
    protected EntityBuilder<TEntity> builder;

    protected String selectByIDSql = "SELECT * FROM " + getTableName() + " WHERE id=?";
    protected String deleteSql = "DELETE FROM " + getTableName() + " WHERE id=?";
    protected String selectAllSql = "SELECT * FROM " + getTableName();

    protected RepositoryImpl(Connection connection, EntityBuilder<TEntity> builder, UnitOfWork uow) {
        this.uow = uow;
        this.builder = builder;
        this.connection = connection;
        try {
            selectByID = connection.prepareStatement(selectByIDSql);
            insert = connection.prepareStatement(getInsertQuery());
            delete = connection.prepareStatement(deleteSql);
            update = connection.prepareStatement(getUpdateQuery());
            selectAll = connection.prepareStatement(selectAllSql);
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public void save(TEntity entity) {
        uow.markAsNew(entity, this);
    }

    public void update(TEntity entity) {
        uow.markAsDirty(entity, this);
    }

    public void delete(TEntity entity) {
        uow.markAsDeleted(entity, this);
    }

    public void persistAdd(Entity entity) {
        try {
            setUpInsertQuery((TEntity) entity);
            insert.executeUpdate();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public void persistUpdate(Entity entity) {
        try {
            setUpUpdateQuery((TEntity) entity);
            update.executeUpdate();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public void persistDelete(Entity entity) {
        try {
            delete.setLong(1, entity.getId());
            delete.executeUpdate();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public TEntity get(Long id) {
        try {
            selectByID.setLong(1, id);
            ResultSet rs = selectByID.executeQuery();
            while (rs.next()) {
                return builder.build(rs);
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }

        return null;
    }

    public List<TEntity> getAll() {
        List<TEntity> result = new ArrayList<>();

        try {
            ResultSet rs = selectAll.executeQuery();
            while (rs.next()) {
                result.add(builder.build(rs));
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }

        return result;
    }

    protected abstract void setUpUpdateQuery(TEntity entity) throws SQLException;

    protected abstract void setUpInsertQuery(TEntity entity) throws SQLException;

    protected abstract String getTableName();

    protected abstract String getUpdateQuery();

    protected abstract String getInsertQuery();
}
