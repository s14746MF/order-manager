package pl.pjwstk.ordermanager.repository;

import pl.pjwstk.ordermanager.builder.EntityBuilder;
import pl.pjwstk.ordermanager.domain.Address;
import pl.pjwstk.ordermanager.unitofwork.UnitOfWork;

import java.sql.Connection;
import java.sql.SQLException;

public class OrderAddressRepository extends RepositoryImpl<Address> {

    protected OrderAddressRepository(Connection connection, EntityBuilder<Address> builder, UnitOfWork uow) {
        super(connection, builder, uow);
    }

    protected void setUpUpdateQuery(Address entity) throws SQLException {
        update.setString(1, entity.getStreet());
        update.setString(2, entity.getBuildingNumber());
        update.setString(3, entity.getFlatNumber());
        update.setString(4, entity.getPostalCode());
        update.setString(5, entity.getCity());
        update.setString(6, entity.getCountry());
        update.setLong(7, entity.getId());
    }

    protected void setUpInsertQuery(Address entity) throws SQLException {
        insert.setString(1, entity.getStreet());
        insert.setString(2, entity.getBuildingNumber());
        insert.setString(3, entity.getFlatNumber());
        insert.setString(4, entity.getPostalCode());
        insert.setString(5, entity.getCity());
        insert.setString(6, entity.getCountry());
    }

    protected String getTableName() {
        return "ORDER_ADDRESSES";
    }

    protected String getUpdateQuery() {
        return "UPDATE ORDER_ADDRESSES SET (street, building_number, flat_number, postal_code, city, country) = (?, ?, ?, ?, ?, ?) WHERE id = ?";
    }

    protected String getInsertQuery() {
        return "INSERT INTO ORDER_ADDRESSES (street, building_number, flat_number, postal_code, city, country) VALUES (?, ?, ?, ?, ?, ?)";
    }
}
