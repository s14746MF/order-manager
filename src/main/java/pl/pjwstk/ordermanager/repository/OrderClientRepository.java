package pl.pjwstk.ordermanager.repository;

import pl.pjwstk.ordermanager.domain.ClientDetails;
import pl.pjwstk.ordermanager.builder.EntityBuilder;
import pl.pjwstk.ordermanager.unitofwork.UnitOfWork;

import java.sql.Connection;
import java.sql.SQLException;

public class OrderClientRepository extends RepositoryImpl<ClientDetails> {

    protected OrderClientRepository(Connection connection, EntityBuilder<ClientDetails> builder, UnitOfWork uow) {
        super(connection, builder, uow);
    }

    protected void setUpUpdateQuery(ClientDetails entity) throws SQLException {
        update.setString(1, entity.getName());
        update.setString(2, entity.getSurname());
        update.setString(3, entity.getLogin());
        update.setLong(4, entity.getId());
    }

    protected void setUpInsertQuery(ClientDetails entity) throws SQLException {
        insert.setString(1, entity.getName());
        insert.setString(2, entity.getSurname());
        insert.setString(3, entity.getLogin());
    }

    protected String getTableName() {
        return "ORDER_CLIENTS";
    }

    protected String getUpdateQuery() {
        return "UPDATE ORDER_CLIENTS SET (name, surname, login) = (?, ?, ?) WHERE id = ?";
    }

    protected String getInsertQuery() {
        return "INSERT INTO ORDER_CLIENTS (name, surname, login) VALUES (?, ?, ?)";
    }
}
