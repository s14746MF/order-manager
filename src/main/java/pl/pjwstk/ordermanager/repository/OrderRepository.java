package pl.pjwstk.ordermanager.repository;

import pl.pjwstk.ordermanager.domain.Order;
import pl.pjwstk.ordermanager.builder.EntityBuilder;
import pl.pjwstk.ordermanager.unitofwork.UnitOfWork;

import java.sql.Connection;
import java.sql.SQLException;

public class OrderRepository extends RepositoryImpl<Order> {

    protected OrderRepository(Connection connection, EntityBuilder<Order> builder, UnitOfWork uow) {
        super(connection, builder, uow);
    }

    protected void setUpUpdateQuery(Order entity) throws SQLException {
        update.setLong(1, entity.getClient().getId());
        update.setLong(2, entity.getDeliveryAddress().getId());
        update.setLong(3, entity.getId());
    }

    protected void setUpInsertQuery(Order entity) throws SQLException {
        insert.setLong(1, entity.getClient().getId());
        insert.setLong(2, entity.getDeliveryAddress().getId());
    }

    protected String getTableName() {
        return "ORDERS";
    }

    protected String getUpdateQuery() {
        return "UPDATE ORDERS SET (client_id, delivery_address_id) = (?, ?) WHERE id = ?";
    }

    protected String getInsertQuery() {
        return "INSERT INTO ORDERS (client_id, delivery_address_id) VALUES (?, ?)";
    }
}
