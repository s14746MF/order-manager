package pl.pjwstk.ordermanager.repository;

import java.util.List;

public interface Repository<TEntity> {

    void save(TEntity entity);

    void update(TEntity entity);

    void delete(TEntity entity);

    TEntity get(Long id);

    List<TEntity> getAll();

}
