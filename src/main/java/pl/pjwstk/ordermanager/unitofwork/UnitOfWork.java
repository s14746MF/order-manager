package pl.pjwstk.ordermanager.unitofwork;

import pl.pjwstk.ordermanager.domain.Entity;

public interface UnitOfWork {

    void commit();

    void rollback();

    void markAsNew(Entity entity, UnitOfWorkRepository repository);

    void markAsDirty(Entity entity, UnitOfWorkRepository repository);

    void markAsDeleted(Entity entity, UnitOfWorkRepository repository);
}