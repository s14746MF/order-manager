package pl.pjwstk.ordermanager.unitofwork;

import pl.pjwstk.ordermanager.domain.Entity;

public interface UnitOfWorkRepository {

    void persistAdd(Entity entity);

    void persistUpdate(Entity entity);

    void persistDelete(Entity entity);
}
