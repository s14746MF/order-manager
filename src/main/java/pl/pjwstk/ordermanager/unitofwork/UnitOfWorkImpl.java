package pl.pjwstk.ordermanager.unitofwork;

import pl.pjwstk.ordermanager.domain.Entity;
import pl.pjwstk.ordermanager.domain.EntityState;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;

public class UnitOfWorkImpl implements UnitOfWork {

    private final Connection connection;
    private final Map<Entity, UnitOfWorkRepository> entities = new LinkedHashMap<Entity, UnitOfWorkRepository>();

    public UnitOfWorkImpl(Connection connection) {
        super();
        this.connection = connection;

        try {
            connection.setAutoCommit(false);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void commit() {
        for (Entity entity : entities.keySet()) {
            switch (entity.getState()) {
                case CHANGED:
                    entities.get(entity).persistUpdate(entity);
                    break;
                case DELETED:
                    entities.get(entity).persistDelete(entity);
                    break;
                case NEW:
                    entities.get(entity).persistAdd(entity);
                    break;
                case UNCHANGED:
                    break;
                default:
                    break;
            }
        }

        try {
            connection.commit();
            entities.clear();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void rollback() {
        entities.clear();
    }

    public void markAsNew(Entity entity, UnitOfWorkRepository repository) {
        entity.setState(EntityState.NEW);
        entities.put(entity, repository);
    }

    public void markAsDirty(Entity entity, UnitOfWorkRepository repository) {
        entity.setState(EntityState.CHANGED);
        entities.put(entity, repository);
    }

    public void markAsDeleted(Entity entity, UnitOfWorkRepository repository) {
        entity.setState(EntityState.DELETED);
        entities.put(entity, repository);
    }
}
