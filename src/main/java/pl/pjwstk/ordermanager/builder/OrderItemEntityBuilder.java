package pl.pjwstk.ordermanager.builder;

import pl.pjwstk.ordermanager.domain.OrderItem;

import java.sql.ResultSet;
import java.sql.SQLException;

public class OrderItemEntityBuilder implements EntityBuilder<OrderItem> {

    @Override
    public OrderItem build(ResultSet resultSet) throws SQLException {
        OrderItem orderItem = new OrderItem();
        orderItem.setId(resultSet.getLong("id"));
        orderItem.setOrderId(resultSet.getLong("order_id"));
        orderItem.setName(resultSet.getString("name"));
        orderItem.setDescription(resultSet.getString("description"));
        orderItem.setPrice(resultSet.getDouble("price"));
        return orderItem;
    }
}
