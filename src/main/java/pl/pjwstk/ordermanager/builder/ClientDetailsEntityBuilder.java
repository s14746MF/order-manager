package pl.pjwstk.ordermanager.builder;

import pl.pjwstk.ordermanager.domain.ClientDetails;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ClientDetailsEntityBuilder implements EntityBuilder<ClientDetails> {

    @Override
    public ClientDetails build(ResultSet resultSet) throws SQLException {
        ClientDetails clientDetails = new ClientDetails();
        clientDetails.setId(resultSet.getLong("id"));
        clientDetails.setName(resultSet.getString("name"));
        clientDetails.setSurname(resultSet.getString("surname"));
        clientDetails.setLogin(resultSet.getString("login"));
        return clientDetails;
    }
}
