package pl.pjwstk.ordermanager.builder;

import pl.pjwstk.ordermanager.domain.Address;
import pl.pjwstk.ordermanager.domain.ClientDetails;
import pl.pjwstk.ordermanager.domain.Order;
import pl.pjwstk.ordermanager.domain.OrderItem;
import pl.pjwstk.ordermanager.repository.OrderAddressRepository;
import pl.pjwstk.ordermanager.repository.OrderClientRepository;
import pl.pjwstk.ordermanager.repository.OrderItemRepository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class OrderEntityBuilder implements EntityBuilder<Order> {

    private final OrderClientRepository orderClientRepository;
    private final OrderAddressRepository orderAddressRepository;
    private final OrderItemRepository orderItemRepository;

    public OrderEntityBuilder(OrderClientRepository orderClientRepository,
                              OrderAddressRepository orderAddressRepository,
                              OrderItemRepository orderItemRepository) {
        this.orderClientRepository = orderClientRepository;
        this.orderAddressRepository = orderAddressRepository;
        this.orderItemRepository = orderItemRepository;
    }

    @Override
    public Order build(ResultSet resultSet) throws SQLException {
        ClientDetails client = orderClientRepository.get(resultSet.getLong("client_id"));
        Address deliveryAddress = orderAddressRepository.get(resultSet.getLong("delivery_address_id"));
        List<OrderItem> orderItems = orderItemRepository.getAllByOrderId(resultSet.getLong("id"));

        Order order = new Order();
        order.setId(resultSet.getLong("id"));
        order.setClient(client);
        order.setDeliveryAddress(deliveryAddress);
        order.addItems(orderItems);
        return order;
    }
}
