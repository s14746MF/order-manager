package pl.pjwstk.ordermanager.builder;

import pl.pjwstk.ordermanager.domain.Entity;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface EntityBuilder<TEntity extends Entity> {

    TEntity build(ResultSet resultSet) throws SQLException;

}
