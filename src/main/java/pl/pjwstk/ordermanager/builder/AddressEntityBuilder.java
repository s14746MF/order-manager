package pl.pjwstk.ordermanager.builder;

import pl.pjwstk.ordermanager.domain.Address;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AddressEntityBuilder implements EntityBuilder<Address> {

    @Override
    public Address build(ResultSet resultSet) throws SQLException {
        Address address = new Address();
        address.setId(resultSet.getLong("id"));
        address.setStreet(resultSet.getString("street"));
        address.setBuildingNumber(resultSet.getString("building_number"));
        address.setFlatNumber(resultSet.getString("flat_number"));
        address.setPostalCode(resultSet.getString("postal_code"));
        address.setCity(resultSet.getString("city"));
        address.setCountry(resultSet.getString("country"));
        return address;
    }
}
