package pl.pjwstk.ordermanager;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionCreator {

    public static Connection createConnection() {
        try {
            return DriverManager.getConnection("jdbc:postgresql://localhost:5432/order_manager", "postgres", "postgres");
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
