package pl.pjwstk.ordermanager;

import pl.pjwstk.ordermanager.domain.Address;
import pl.pjwstk.ordermanager.domain.ClientDetails;
import pl.pjwstk.ordermanager.domain.Order;
import pl.pjwstk.ordermanager.domain.OrderItem;

public class OrderCreator {

    public static Order createSampleOrder() {
        ClientDetails clientDetails = createClientDetails();

        Address deliveryAddress = createAddress();

        OrderItem orderItem1 = createOrderItem();

        OrderItem orderItem2 = new OrderItem();
        orderItem2.setName("Płaszcz 2");
        orderItem2.setDescription("Przeciwdeszczowy 2");
        orderItem2.setPrice(80.00);

        Order order = new Order();
        order.setClient(clientDetails);
        order.setDeliveryAddress(deliveryAddress);
        order.addItem(orderItem1);
        order.addItem(orderItem2);

        return order;
    }

    public static OrderItem createOrderItem() {
        OrderItem orderItem1 = new OrderItem();
        orderItem1.setOrderId(1L);
        orderItem1.setName("Kalosze 2");
        orderItem1.setDescription("Super kalosze w kropki 2");
        orderItem1.setPrice(100.00);
        return orderItem1;
    }

    public static Address createAddress() {
        Address deliveryAddress = new Address();
        deliveryAddress.setStreet("Mickiewicza");
        deliveryAddress.setBuildingNumber("10");
        deliveryAddress.setFlatNumber("5");
        deliveryAddress.setPostalCode("80-216");
        deliveryAddress.setCity("Gdańsk");
        deliveryAddress.setCountry("Polska");
        return deliveryAddress;
    }

    public static ClientDetails createClientDetails() {
        ClientDetails clientDetails = new ClientDetails();
        clientDetails.setName("Jan");
        clientDetails.setSurname("Kowalski");
        clientDetails.setLogin("j.kowalski");
        return clientDetails;
    }
}
