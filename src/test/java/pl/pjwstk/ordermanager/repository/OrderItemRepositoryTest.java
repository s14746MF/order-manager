package pl.pjwstk.ordermanager.repository;

import org.junit.Test;
import pl.pjwstk.ordermanager.ConnectionCreator;
import pl.pjwstk.ordermanager.OrderCreator;
import pl.pjwstk.ordermanager.builder.OrderItemEntityBuilder;
import pl.pjwstk.ordermanager.domain.OrderItem;
import pl.pjwstk.ordermanager.unitofwork.UnitOfWork;
import pl.pjwstk.ordermanager.unitofwork.UnitOfWorkImpl;

import java.sql.Connection;

public class OrderItemRepositoryTest {

    private final Connection connection = ConnectionCreator.createConnection();
    private final OrderItemEntityBuilder orderItemEntityBuilder = new OrderItemEntityBuilder();
    private final UnitOfWork unitOfWork = new UnitOfWorkImpl(connection);
    private final OrderItemRepository orderItemRepository = new OrderItemRepository(connection, orderItemEntityBuilder, unitOfWork);

    @Test
    public void shouldInsertOrderItem() throws Exception {
        // given
        OrderItem orderItem = OrderCreator.createOrderItem();

        // when
        orderItemRepository.save(orderItem);
        unitOfWork.commit();
    }
}