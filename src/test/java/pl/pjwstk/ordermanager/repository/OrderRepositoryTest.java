package pl.pjwstk.ordermanager.repository;

import org.junit.Test;
import pl.pjwstk.ordermanager.ConnectionCreator;
import pl.pjwstk.ordermanager.OrderCreator;
import pl.pjwstk.ordermanager.builder.AddressEntityBuilder;
import pl.pjwstk.ordermanager.builder.ClientDetailsEntityBuilder;
import pl.pjwstk.ordermanager.builder.OrderEntityBuilder;
import pl.pjwstk.ordermanager.builder.OrderItemEntityBuilder;
import pl.pjwstk.ordermanager.domain.Order;
import pl.pjwstk.ordermanager.unitofwork.UnitOfWork;
import pl.pjwstk.ordermanager.unitofwork.UnitOfWorkImpl;

import java.sql.Connection;

import static org.junit.Assert.assertEquals;

public class OrderRepositoryTest {

    private final Connection connection = ConnectionCreator.createConnection();
    private final UnitOfWork unitOfWork = new UnitOfWorkImpl(connection);

    private final ClientDetailsEntityBuilder clientDetailsEntityBuilder = new ClientDetailsEntityBuilder();
    private final OrderClientRepository orderClientRepository = new OrderClientRepository(connection, clientDetailsEntityBuilder, unitOfWork);

    private final AddressEntityBuilder addressEntityBuilder = new AddressEntityBuilder();
    private final OrderAddressRepository orderAddressRepository = new OrderAddressRepository(connection, addressEntityBuilder, unitOfWork);

    private final OrderItemEntityBuilder orderItemEntityBuilder = new OrderItemEntityBuilder();
    private final OrderItemRepository orderItemRepository = new OrderItemRepository(connection, orderItemEntityBuilder, unitOfWork);

    private final OrderEntityBuilder orderEntityBuilder = new OrderEntityBuilder(orderClientRepository, orderAddressRepository, orderItemRepository);
    private final OrderRepository orderRepository = new OrderRepository(connection, orderEntityBuilder, unitOfWork);

    @Test
    public void shouldInsertOrder() throws Exception {
        // given
        Order sampleOrder = OrderCreator.createSampleOrder();

        // when
        orderRepository.save(sampleOrder);
        unitOfWork.commit();
    }

    @Test
    public void shouldGetOrder() throws Exception {
        // given
        Long orderId = 1L;

        // when
        Order order = orderRepository.get(orderId);

        // then
        assertEquals(orderId, order.getId());
    }
}