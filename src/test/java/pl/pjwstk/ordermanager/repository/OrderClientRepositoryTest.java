package pl.pjwstk.ordermanager.repository;

import org.junit.Test;
import pl.pjwstk.ordermanager.ConnectionCreator;
import pl.pjwstk.ordermanager.OrderCreator;
import pl.pjwstk.ordermanager.builder.ClientDetailsEntityBuilder;
import pl.pjwstk.ordermanager.domain.ClientDetails;
import pl.pjwstk.ordermanager.unitofwork.UnitOfWork;
import pl.pjwstk.ordermanager.unitofwork.UnitOfWorkImpl;

import java.sql.Connection;

public class OrderClientRepositoryTest {

    private final Connection connection = ConnectionCreator.createConnection();
    private final ClientDetailsEntityBuilder clientDetailsEntityBuilder = new ClientDetailsEntityBuilder();
    private final UnitOfWork unitOfWork = new UnitOfWorkImpl(connection);
    private final OrderClientRepository orderClientRepository = new OrderClientRepository(connection, clientDetailsEntityBuilder, unitOfWork);

    @Test
    public void shouldInsertOrderClient() throws Exception {
        // given
        ClientDetails clientDetails = OrderCreator.createClientDetails();

        // when
        orderClientRepository.save(clientDetails);
        unitOfWork.commit();
    }
}