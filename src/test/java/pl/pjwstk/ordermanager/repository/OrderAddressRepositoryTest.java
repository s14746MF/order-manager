package pl.pjwstk.ordermanager.repository;

import org.junit.Test;
import pl.pjwstk.ordermanager.ConnectionCreator;
import pl.pjwstk.ordermanager.OrderCreator;
import pl.pjwstk.ordermanager.builder.AddressEntityBuilder;
import pl.pjwstk.ordermanager.domain.Address;
import pl.pjwstk.ordermanager.unitofwork.UnitOfWork;
import pl.pjwstk.ordermanager.unitofwork.UnitOfWorkImpl;

import java.sql.Connection;

public class OrderAddressRepositoryTest {

    private final Connection connection = ConnectionCreator.createConnection();
    private final AddressEntityBuilder addressEntityBuilder = new AddressEntityBuilder();
    private final UnitOfWork unitOfWork = new UnitOfWorkImpl(connection);
    private final OrderAddressRepository orderAddressRepository = new OrderAddressRepository(connection, addressEntityBuilder, unitOfWork);

    @Test
    public void shouldInsertOrderAddress() throws Exception {
        // given
        Address address = OrderCreator.createAddress();

        // when
        orderAddressRepository.save(address);
        unitOfWork.commit();
    }
}